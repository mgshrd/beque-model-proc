Module PosixProcessMixActions
.

Inductive posixprocessmix_action :=.

Definition eq_posixprocessmix_action_dec :
  forall a1 a2 : posixprocessmix_action,
    {a1 = a2}+{a1 <> a2}.
Proof.
  decide equality.
Defined.

Definition posixprocessmix_action_res
           (a : posixprocessmix_action) :
  Set :=
  match a with
  end.

Definition eq_posixprocessmix_action_res_dec :
  forall (a : posixprocessmix_action) (res1 res2 : posixprocessmix_action_res a),
    {res1 = res2}+{res1 <> res2}.
Proof.
  intros.
  destruct a; simpl in *; decide equality.
Defined.

End PosixProcessMixActions.
