Require Import Beque.Util.sublist.
Require Import Beque.Util.dec_utils.
Require Import Beque.Util.ltac_utils.
Require Import Beque.Util.DelayExtractionDefinitionToken.

Require Import IO.intf.World.
Require Import IO.intf.IORaw.
Require Import IO.intf.IOAux.

Require Import LTL.util.LTL.

Require Import Beque.Model.Argv.impl.ArgvImpl.

Require Import Beque.Model.Env.impl.EnvImpl.

Require Import Beque.Model.FS.intf.FDActions.
Require Import Beque.Model.FS.impl.FDHandleImpl.
Require Import Beque.Model.FS.impl.FDImpl.
Require Import Beque.Model.FS.impl.FSActionsImpl.
Require Import Beque.Model.FS.impl.FSImpl.

Require Import Beque.Model.Log.impl.LogImpl.

Require Import PosixProcessMix.

(** Posix process model implementation *)
Module PosixProcessMixImpl
       (w : World)
       (ior : IORaw w)
.

Module ArgvX.
Include ArgvImpl w ior.
End ArgvX.

Module EnvX.
Include EnvImpl w ior.
End EnvX.

Module FDImplX.
Include FDImpl w ior FDHandleImpl.
End FDImplX.
Module FSActionsImplX.
Include FSActionsImpl w ior FDHandleImpl FDImplX.
End FSActionsImplX.
Module FSImplX.
Include FSImpl w ior FDHandleImpl FDImplX FSActionsImplX.
End FSImplX.

Module LogX.
Include LogImpl w ior.
End LogX.

Include PosixProcessMix w ior ArgvX EnvX FDHandleImpl FDImplX FSActionsImplX FSImplX LogX.

Module extractHaskell (token : DelayExtractionDefinitionToken).

Module argve := ArgvX.extractHaskell DelayExtractionDefinitionToken.token.

Module enve := EnvX.extractHaskell DelayExtractionDefinitionToken.token.

Module fdhe := FDHandleImpl.extractHaskell DelayExtractionDefinitionToken.token.
Module fda := FDActions FDHandleImpl.
Module fde := FDImplX.extractHaskell DelayExtractionDefinitionToken.token.
Module fse := FSImplX.extractHaskell DelayExtractionDefinitionToken.token.

Module loge := LogX.extractHaskell DelayExtractionDefinitionToken.token.

Extract Constant start_state => "Prelude.return ()".

End extractHaskell.

Module extractOcaml (token : DelayExtractionDefinitionToken).

Module argve := ArgvX.extractOcaml DelayExtractionDefinitionToken.token.

Module enve := EnvX.extractOcaml DelayExtractionDefinitionToken.token.

Module fdhe := FDHandleImpl.extractOcaml DelayExtractionDefinitionToken.token.
Module fda := FDActions FDHandleImpl.
Module fde := FDImplX.extractOcaml DelayExtractionDefinitionToken.token.
Module fse := FSImplX.extractOcaml DelayExtractionDefinitionToken.token.

Module loge := LogX.extractOcaml DelayExtractionDefinitionToken.token.

Extract Constant start_state => "fun () -> ()".

End extractOcaml.

End PosixProcessMixImpl.
