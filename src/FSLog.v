Require Import Beque.Util.DelayExtractionDefinitionToken.

Require Import IO.intf.World.
Require Import IO.intf.IORaw.

Require Import Beque.Frame.intf.array.ElemHandle.
Require Import Beque.Model.FS.intf.FD.
Require Import Beque.Model.FS.intf.FSActions.
Require Import Beque.Model.FS.intf.FS.

Require Import Beque.Model.Log.Log.

Require Import Beque.Frame.intf.two.TwoIndependentDomains.

(** Domain of two independent domains. *)
Module FSLog
       (w : World)
       (ior : IORaw w)

       (subid : ElemHandle)
       (subd : FD w ior subid)
       (daa : FSActions w ior subid subd)
       (fs : FS w ior subid subd daa)

       (log : Log w ior)
.

Module fl := TwoIndependentDomains w ior fs log.
Include fl.

Module extractHaskell (token : DelayExtractionDefinitionToken).

Module fse := fs.extractHaskell token.
Include fse.

Module loge := log.extractHaskell token.
Include loge.

End extractHaskell.

Module extractOcaml (token : DelayExtractionDefinitionToken).

Module fse := fs.extractOcaml token.
Include fse.

Module loge := log.extractOcaml token.
Include loge.

End extractOcaml.

End FSLog.
