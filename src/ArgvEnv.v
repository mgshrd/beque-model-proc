Require Import Beque.Util.DelayExtractionDefinitionToken.

Require Import IO.intf.World.
Require Import IO.intf.IORaw.

Require Import Beque.Frame.intf.two.TwoIndependentDomains.

Require Import Beque.Model.Argv.Argv.
Require Import Beque.Model.Env.Env.

Module ArgvEnv
       (w : World)
       (ior : IORaw w)

       (argv : Argv w ior)
       (env : Env w ior)
.

Module ae := TwoIndependentDomains w ior argv env.
Include ae.

Module extractHaskell (token : DelayExtractionDefinitionToken).

Module argve := argv.extractHaskell token.
Include argve.

Module enve := env.extractHaskell token.
Include enve.

End extractHaskell.

Module extractOcaml (token : DelayExtractionDefinitionToken).

Module argve := argv.extractOcaml token.
Include argve.

Module enve := env.extractOcaml token.
Include enve.

End extractOcaml.

End ArgvEnv.
