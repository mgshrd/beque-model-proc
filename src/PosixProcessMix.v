Require Import IO.intf.World.
Require Import IO.intf.IORaw.

Require Import TwoIndependentDomains.

Require Import Beque.Model.Argv.Argv.
Require Import Beque.Model.Env.Env.
Require Import ArgvEnv.

Require Import Beque.Frame.intf.array.ElemHandle.
Require Import Beque.Model.FS.intf.FD.
Require Import Beque.Model.FS.intf.FSActions.
Require Import Beque.Model.FS.intf.FS.

Require Import Beque.Model.Log.Log.
Require Import FSLog.

(** Domain of two independent domains. *)
Module PosixProcessMix
       (w : World)
       (ior : IORaw w)

       (argv : Argv w ior)

       (env : Env w ior)

       (subid : ElemHandle)
       (subd : FD w ior subid)
       (daa : FSActions w ior subid subd)
       (fs : FS w ior subid subd daa)

       (log : Log w ior)
.

Module ae := ArgvEnv w ior argv env.
Module fl := FSLog w ior subid subd daa fs log.
Module aefl := TwoIndependentDomains w ior ae fl.
Include aefl.

Axiom new_proc : list w.msg -> Prop.
Axiom new_proc_props :
  forall t,
    new_proc t ->
    (forall (fd : subid.handle) (n : daa.name), ~fs.isopenfor fd n t).
Axiom start_state : ior.IO (fun t => new_proc t) unit (fun _ s _ => s = nil). (* TODO broken (can pretend to restart anytime) *)

End PosixProcessMix.
