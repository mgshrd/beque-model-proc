Require Import List.

Require Import ltac_utils.

Require Beque.Util.common_extract.
Require Beque.Util.DelayExtractionDefinitionToken.

Require Import Beque.IO.impl.AxiomaticIOInAUnitWorld.
Require Import Beque.IO.intf.IOAux.
Require Import Beque.IO.util.IODebug.
Require Import Beque.IO.util.ResultUtils.

Require Import PosixProcessMixImpl.

Extraction Language Haskell.

Module comext.
Include common_extract.commonExtractHaskell DelayExtractionDefinitionToken.token.
End comext.

Module ioext.
Include AxiomaticIOInAUnitWorld.extractHaskell DelayExtractionDefinitionToken.token.
End ioext.

Module iod := IODebug UnitWorld AxiomaticIOInAUnitWorld.
Import iod.
Module ru := ResultUtils UnitWorld AxiomaticIOInAUnitWorld.
Import ru.

Module posixprocessmix.
Include PosixProcessMixImpl AxiomaticIOInAUnitWorld.UnitWorld AxiomaticIOInAUnitWorld.
End posixprocessmix.
Module ppe := posixprocessmix.extractHaskell DelayExtractionDefinitionToken.token.

Definition log_error := (posixprocessmix.LogX.log log_level.log_level_error).
Arguments log_error _%string_scope.
Definition log_info  := (posixprocessmix.LogX.log log_level.log_level_info).
Arguments log_info _%string_scope.
Definition scid {A} (a : A) := a.
Arguments scid {_} _%string_scope.
Program Definition main :=
  _ <== posixprocessmix.start_state;;
    args <-- posixprocessmix.ArgvX.get_args;;
    fn <?-- IO_return (match args with
                       | nil => result.err tt
                       | h::_ => result.ok h
                       end) [err /| log_error "usage: <prog> <file>"];;
    fd_e <?-- posixprocessmix.FSImplX.open fn [err /| log_error (fold_right String.append (scid "") (scid "failed to open "::fn::scid ": "::fd_e::nil))];;
    fd <-- IO_return fd_e;;
    s <?-- posixprocessmix.FSImplX.read fd 10 [err _ <-- posixprocessmix.FSImplX.close fd;; /| log_error (String.append "read failed " fn)];;
    _ <-- posixprocessmix.LogX.log log_level.log_level_info s;;
    /| posixprocessmix.FSImplX.close fd.
Next Obligation.
  simpl in *.
  assert (newprocpf := posixprocessmix.new_proc_props _ H12).
  match goal with |- context[H3 ++ ?t'] => remember t' as t end.
  rename H3 into getargseg; rename H9 into getargsegpf.
  assert (Hx := newprocpf h fn). clear newprocpf; rename Hx into newprocpf.
  intro notopen; elim newprocpf; clear newprocpf. revert notopen getargsegpf; clear; intros.
  assert (Hy := @posixprocessmix.back_propagates_correct).
  unfold posixprocessmix.action in Hy.
  unfold posixprocessmix.ae.action in Hy.
  assert (Hx := fun p => Hy p (inl (inl posixprocessmix.ArgvX.aam.argv_action__get_args)) args). clear Hy. (*TODO lifts*)
  unfold posixprocessmix.segment_of_action_with_res in Hx.
  unfold posixprocessmix.ae.segment_of_action_with_res in Hx.
  assert (Hy := fun p ppf => Hx p ppf _ getargsegpf t). clear Hx.
  unfold posixprocessmix.back_propagates in Hy.
  unfold posixprocessmix.d1_action_back_idempotent_for_d2_prop in Hy.
  unfold posixprocessmix.ae.back_propagates in Hy.
  unfold posixprocessmix.ae.d1_action_back_idempotent_for_d2_prop in Hy.
  unfold posixprocessmix.prop in Hy.
  unfold posixprocessmix.fl.prop in Hy.
  unfold posixprocessmix.FSImplX.isopenfor.
  assert (Hx := Hy (inr (inl (posixprocessmix.FSImplX.dw_prop_isopenfor h fn))) I). clear Hy.
  simpl in Hx.
  apply Hx.
  assumption.
Qed.
Next Obligation.
  unfold posixprocessmix.FSImplX.isvalidref. exists fn. assumption.
Qed.
Next Obligation.
  unfold posixprocessmix.FSImplX.isvalidref. exists fn.
  rename H1 into logseg. rename H5 into logsegpf.
  rename H4 into readseg. rename H9 into readsegpf.
  match goal with |- context[H12 ++ ?t'] => remember (H12 ++ t') as openpast end. rename H19 into openpf.
  unfold posixprocessmix.FSImplX.isopenfor in *.

  assert (ppf := fun pf => @posixprocessmix.FSImplX.propagates_correct _ _ _ pf _ readsegpf _ openpf).
  unfold posixprocessmix.FSImplX.propagates in ppf. assert (ppf' := ppf I). clear ppf.

  assert (Hx := @posixprocessmix.propagates_correct).
  assert (Hx' := Hx (inr (inl (posixprocessmix.FSImplX.dw_prop_isopenfor fd_e fn)))); clear Hx; rename Hx' into Hx.
  assert (Hx' := Hx (inr (inr (posixprocessmix.LogX.lam.log_action__log log_level.log_level_info s)))); clear Hx; rename Hx' into Hx.
  assert (Hx' := Hx H0); clear Hx; rename Hx' into Hx.
  unfold posixprocessmix.propagates in Hx. unfold posixprocessmix.fl.propagates in Hx. unfold posixprocessmix.fl.d2_action_idempotent_for_d1_prop in Hx.
  assert (Hx' := Hx I); clear Hx; rename Hx' into Hx.
  assert (Hx' := Hx _ logsegpf); clear Hx; rename Hx' into Hx.

  apply (Hx _ ppf').
Qed.
Next Obligation.
  unfold posixprocessmix.FSImplX.isvalidref. exists fn.
  rename H0 into readseg; rename H4 into readsegpf.
  rename H14 into openpf.
  match goal with |- context[H7 ++ ?t'] => remember (H7 ++ t') as openpast end.
  unfold posixprocessmix.FSImplX.isopenfor in *.
  assert (ppf := fun pf => @posixprocessmix.FSImplX.propagates_correct _ _ _ pf _ readsegpf _ openpf).
  unfold posixprocessmix.FSImplX.propagates in ppf. assert (ppf' := ppf I). clear ppf.
  exact ppf'.
Qed.

Extraction "test" main.
